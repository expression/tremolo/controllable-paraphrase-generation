import torch, time, sys, argparse, os, codecs, h5py, csv
import numpy as np
import pickle as cPickle
from torch.autograd import Variable
from train_cpgn_expattern import SCPN
from subwordnmt.apply_bpe import BPE, read_vocabulary

# reload(sys)
# sys.setdefaultencoding('utf8')

h5f = h5py.File('data/parsed_data.h5', 'r')
inp = h5f['inputs']
out = h5f['outputs']
in_lens = h5f['in_lengths']
out_lens = h5f['out_lengths']
print ('data is loaded')

test_indices = []
ref_file = open('evaluation4k/ref_paranmt_pattern_rev_4k_2.txt', 'w')
inp_file = open('evaluation4k/inp_paranmt_pattern_rev_4k_2.txt', 'w')
hyp_file = open('evaluation4k/hyp_pattern_paranmt_rev_4k_2.txt', 'w')
template_file = open('evaluation4k/template_pattern_rev_paranmt_4k_2.txt', 'w')


def reverse_bpe(sent):
    x = []
    cache = ''

    for w in sent:
        if w.endswith('@@'):
            cache += w.replace('@@', '')
        elif cache != '':
            x.append(cache + w)
            cache = ''
        else:
            x.append(w)

    return ' '.join(x)


# encode sentences and parses for targeted paraphrasing
def encode_data(out_f, tp_templates, tp_template_lens, fold):


    ref_dic = {}
    for i in fold:
        eos = np.where(out[i] == pp_vocab['EOS'])[0][0]
        ssent = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(out[i, :eos])\
                        if j < out_lens[i]-1])

        ref_dic[i]=ssent
        ref_file.write(ssent)
        ref_file.write('\n')


    # read parsed data
    # infile = codecs.open(args.parsed_input_file, 'r', 'utf-8')
    # inrdr = csv.DictReader(infile, delimiter='\t')

    # loop over sentences and transform them
    cnt = 0
    for i in fold:
        # if cnt > CNT:
        #     break
        cnt += 1

        stime = time.time()
        input_sentence = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(inp[i])\
                        if j < in_lens[i]-1])
        inp_file.write(input_sentence)
        inp_file.write('\n')

        print ('\n ============ {} ============= '.format(cnt))
        print ('input: {}'.format(input_sentence))
        # write gold sentence
        # out_f.writerow({'idx': i,
        #               'template': 'GOLD', 'generated_length': len(input_sentence.split()),
        #               'sentence': input_sentence})

        torch_sent = Variable(torch.from_numpy(np.array(inp[i], dtype='int32')).long().cuda())
        torch_sent_len = torch.from_numpy(np.array([in_lens[i]], dtype='int32')).long().cuda()

        # generate paraphrases from parses
        try:
            beam_dict = net.batch_beam_search(torch_sent.unsqueeze(0), tp_templates,
                                              torch_sent_len[:], tp_template_lens, pp_vocab['EOS'], beam_size=3,
                                              max_steps=40)

            for b_idx in beam_dict:
                prob, _, _, seq = beam_dict[b_idx][0]

                # ##################
                # new gen EOF handling
                gen_sent_list= []
                for w in seq[:-1]:
                    if rev_pp_vocab[w] == 'EOS':
                        break
                    gen_sent_list.append(rev_pp_vocab[w])
                gen_sent = ' '.join(gen_sent_list)
                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                gen_length = len(gen_sent_list)

                # ##################
                # out_f.writerow({'idx': i,
                #               'template': templates_length[b_idx], 'generated_length': gen_length,
                #               'sentence': gen_sent})

                if b_idx == cnt - 1:
                    # print ('===============================================\n')
                    print ('ref: {}'.format(ref_dic[i]))
                    print ('template: {}'.format(templates_length[b_idx]))
                    print ('generated: {}'.format(gen_sent))
                # if len(ref_list[cnt - 1].split()) == tt:
                    hyp_file.write(gen_sent)
                    hyp_file.write('\n')
                    hyp_file.flush()

                    template_file.write(templates_length[b_idx])
                    template_file.write('\n')
                    template_file.flush()


                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                # out.writerow({'idx': ex['idx'],
                #     'template':tt, 'generated_length':len(seq[:-1]),
                #     'sentence':reverse_bpe(gen_sent.split())})

        except Exception as e:
            print (e)
            print ('beam search OOM')

        # print i, time.time() - stime



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Syntactically Controlled Paraphrase Transformer')

    ## paraphrase model args
    parser.add_argument('--gpu', type=str, default='1',
                        help='GPU id')
    parser.add_argument('--out_file', type=str, default='output4k/scpn_blank_2_paranmt_rev_4k_2.out',
                        help='paraphrase save path')
    parser.add_argument('--parsed_input_file', type=str, default='data/parsed_data.h5',
                        help='parse load path')
    # parser.add_argument('--parsed_input_file', type=str, default='data/paws.csv',
    #         help='parse load path')
    parser.add_argument('--vocab', type=str, default='data/parse_vocab.pkl',
                        help='word vocabulary')
    parser.add_argument('--pp_model', type=str, default='models/scpn2_pattern_2_reverse4k_.pt',
                        help='paraphrase model to load')

    ## BPE args
    parser.add_argument('--bpe_codes', type=str, default='data/bpe.codes')
    parser.add_argument('--bpe_vocab', type=str, default='data/vocab.txt')
    parser.add_argument('--bpe_vocab_thresh', type=int, default=50)

    args = parser.parse_args()

    # os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    modell = args.pp_model
    slash = modell.find('/')+1
    modelll_name = modell[slash :]
    with open('minibatches/test_minibatches_' + modelll_name + '.txt', 'r') as f:
        for line in f.readlines():
            [start, end] = [int(x) for x in line.strip().split(',')]
            for i in range(start, end):
                test_indices.append(i)

    length = int(len(test_indices) / 200)  # length of each fold : 64
    folds = []
    for i in range(199):
        folds += [test_indices[i * length:(i + 1) * length]]
    folds += [test_indices[199 * length:len(test_indices)]]

    print ('len(folds): {}'.format(len(folds)))





    # load saved models
    pp_model = torch.load(args.pp_model)

    # load vocab
    pp_vocab, rev_pp_vocab = cPickle.load(open(args.vocab, 'rb'))

    # tag_file = codecs.open(args.parse_vocab, 'r', 'utf-8')
    # parse_gen_voc = {}
    # for idx, line in enumerate(tag_file):
    #     line = line.strip()
    #     parse_gen_voc[line] = idx
    # rev_label_voc = dict((v, k) for (k, v) in parse_gen_voc.iteritems())

    # load paraphrase network
    pp_args = pp_model['config_args']
    # print 'pp_args.d_nt: {}'.format(pp_args.d_nt)
    # print 'len(parse_gen_voc) - 1: {}'.format(len(parse_gen_voc) - 1)
    net = SCPN(pp_args.d_word, pp_args.d_hid, pp_args.d_nt, pp_args.d_trans,
               len(pp_vocab), len(pp_vocab), pp_args.use_input_parse, None)
    net.cuda()
    net.load_state_dict(pp_model['state_dict'])
    net.eval()

    fn = ['idx', 'template', 'generated_length', 'sentence']
    ofile = codecs.open(args.out_file, 'w', 'utf-8')

    out_f = csv.DictWriter(ofile, delimiter='\t', fieldnames=fn)
    out_f.writerow(dict((x, x) for x in fn))

    for fold in folds:
        # encode templates
        # template_lens = ['___ ___ ___ ___ ___' for x in range(CNT)]
        templates_length = []

        # counter = 0

        for i in list(fold):
            input_i = inp[i]
            output_i = out[i]

            flag_j = False
            flag_k = False
            for j in range(in_lens[i]):
                if input_i[j] not in output_i:
                    flag_j = True
                    for k in range(j + 1, in_lens[i]):
                        if input_i[k] not in output_i:
                            flag_k = True

                            res = rev_pp_vocab[input_i[j]] + ' ' + rev_pp_vocab[input_i[k]]

                            break

                    if not flag_k:
                        res = rev_pp_vocab[input_i[j]]
                    break

            if not flag_j:
                fold.remove(i)
                continue
                # res = ' '

            templates_length.append(str(res))

        np_templates = np.zeros((len(fold), 3), dtype='int32')

        template_lens = [len(x.split(' ')) for x in templates_length]
        # print ('template_lens: {}'.format(template_lens))
        # print ('templates_length: {}'.format(len(templates_length)))
        # print ('shape: {}'.format(np_templates.shape))
        for z, template in enumerate(templates_length):
            # print ('template_lens[z]: {}'.format(template_lens[z]))
            np_templates[z, :template_lens[z]] = [pp_vocab[w] for w in templates_length[z].split(' ')]

        tp_templates = Variable(torch.from_numpy(np_templates).long().cuda())
        tp_template_lens = torch.from_numpy(np.array(template_lens, dtype='int32')).long().cuda()

        # instantiate BPE segmenter
        bpe_codes = codecs.open(args.bpe_codes, encoding='utf-8')
        bpe_vocab = codecs.open(args.bpe_vocab, encoding='utf-8')
        bpe_vocab = read_vocabulary(bpe_vocab, args.bpe_vocab_thresh)
        bpe = BPE(bpe_codes, '@@', bpe_vocab, None)

        # paraphrase the sst!
        encode_data(out_f, tp_templates, tp_template_lens, fold)

    hyp_file.close()
    template_file.close()
    ref_file.close()
    inp_file.close()
