import torch, time, sys, argparse, os, codecs, h5py, csv
import numpy as np
from torch.autograd import Variable
from nltk import ParentedTree
from train_cpgn_length_bertscore import CPGN
from subwordnmt.apply_bpe import BPE, read_vocabulary
import pickle as cPickle

h5f = h5py.File('data/parsed_data.h5', 'r')
inp = h5f['inputs']
out = h5f['outputs']
in_lens = h5f['in_lengths']
out_lens = h5f['out_lengths']

scores_list = cPickle.load(open('scores_list_scalar.pkl', 'rb'))
in_bert = scores_list

print ('data is loaded')

test_indices = []
ref_file = open('evaluation4k/ref_paranmt_pattern_B_4k_2f_m_again.txt', 'w')
inp_file = open('evaluation4k/inp_paranmt_pattern_B_4k_2f_m_again.txt', 'w')
hyp_file = open('evaluation4k/hyp_pattern_paranmt_B_4k_2f_m_again.txt', 'w')

# encode sentences and parses for targeted paraphrasing
def encode_data(out_f, tp_templates_b, tp_templates_l, tp_template_lens_b, tp_template_lens_l, fold):
    ref_dic = {}
    for i in fold:
        eos = np.where(out[i] == pp_vocab['EOS'])[0][0]
        ssent = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(out[i, :eos])\
                        if j < out_lens[i]-1])

        ref_dic[i]=ssent
        ref_file.write(ssent)
        ref_file.write('\n')

    # loop over sentences and transform them
    cnt = 0
    for i in fold:
        # if cnt > CNT:
        #     break
        cnt += 1

        stime = time.time()
        input_sentence = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(inp[i])\
                        if j < in_lens[i]-1])
        inp_file.write(input_sentence)
        inp_file.write('\n')
        inp_file.flush()

        print ('\n ============ {}, {} ============= '.format(cnt, i))
        print ('input: {}'.format(input_sentence))

        F1 = round(in_bert[i],3)
        # print ('Gold F1: {}'.format(F1))
        absolute_difference_function = lambda list_value: abs(list_value - F1)
        closest_value = min([x[0][0] for x in templates_bl], key=absolute_difference_function)
        # print ('closest_value: {}'.format(closest_value))

        # out_f.writerow({'idx': i,
        #               'template_B': 'GOLD', 'template_L': 'GOLD', 'result_l': len(input_sentence.split()),
        #               'sentence': input_sentence})
        # ofile.flush()


        torch_sent = Variable(torch.from_numpy(np.array(inp[i], dtype='int32')).long().cuda())
        torch_sent_len = torch.from_numpy(np.array([in_lens[i]], dtype='int32')).long().cuda()

        # generate paraphrases from parses
        try:

            beam_dict = net.batch_beam_search(torch_sent.unsqueeze(0), tp_templates_b, tp_templates_l,
                torch_sent_len[:], tp_template_lens_b, tp_template_lens_l, pp_vocab['EOS'], beam_size=3, max_steps=40)

            for b_idx in beam_dict:
                prob,_,_,seq = beam_dict[b_idx][0]
                tt_b = templates_bl[b_idx][0][0]
                tt_l = templates_bl[b_idx][1][0]

                # ##################
                # new gen EOF handling

                gen_sent_list= []
                for w in seq[:-1]:
                    if rev_pp_vocab[w] == 'EOS':
                        break
                    gen_sent_list.append(rev_pp_vocab[w])
                gen_sent = ' '.join(gen_sent_list)
                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])

                # ##################
                # P, R, F1 = score([gen_sent], [input_sentence], lang='en', verbose=False)
                # F1 = round(float(F1[0]), 3)
                F1 = '0'
                r_l = len(gen_sent_list)
                # print (' ')
                # print ('GEN: {}'.format(gen_sent))
                # print ('length: {}'.format(r_l))
                # print ('INP: {}'.format(input_sentence))
                # print ('Template B: {}'.format(tt_b))
                # print ('Template L: {}'.format(tt_l))
                # print ('F1: {}'.format(F1))

                # out_f.writerow({'idx': i,
                #     'template_B': tt_b,
                #     'template_L': tt_l,
                #     'result_l': r_l,
                #     'result': result_bert_score,
                    # 'sentence':gen_sent})


                # if int(out_lens_i) == int(tt_l):
                #     print ('{}, {}\t{}, {}'.format(closest_value,tt_b, out_lens_i, tt_l))

                # if float(closest_value) == float(tt_b) and int(out_lens[i]) == int(tt_l):
                if b_idx == cnt - 1:

                    print ('\n yes \n')
                    hyp_file.write(gen_sent)
                    hyp_file.write('\n')
                    hyp_file.flush()

        except Exception as e:
            print (e)

        print (i, time.time() - stime)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Syntactically Controlled Paraphrase Transformer')

    ## paraphrase model args
    parser.add_argument('--gpu', type=str, default='0',
            help='GPU id')
    parser.add_argument('--out_file', type=str, default='output4k/CPGN2_bert_scalar_emb_B_L_4k_2f_m.out',
            help='paraphrase save path')
    parser.add_argument('--parsed_input_file', type=str, default='data/parsed_data.h5',
            help='parse load path')
    parser.add_argument('--vocab', type=str, default='data/parse_vocab.pkl',
            help='word vocabulary')
    parser.add_argument('--parse_vocab_1', type=str, default='data/unique_bert_scalar.txt',
            help='tag vocabulary')
    parser.add_argument('--parse_vocab_2', type=str, default='data/unique_lenght.txt',
            help='tag vocabulary')
    parser.add_argument('--pp_model', type=str, default='models/CPGN2_L_B_new_scalaremb_4k_again.pt',
            help='paraphrase model to load')

    ## BPE args
    parser.add_argument('--bpe_codes', type=str, default='data/bpe.codes')
    parser.add_argument('--bpe_vocab', type=str, default='data/vocab.txt')
    parser.add_argument('--bpe_vocab_thresh', type=int, default=50)

    args = parser.parse_args()

    with open('paranmt_test_train_dev.pickle', 'rb') as f:
        test_indices = cPickle.load(f)

    print ('len((test_indices)): {}'.format(len((test_indices))))

    length = int(len(test_indices) / 400)  # length of each fold : 64
    folds = []
    for i in range(399):
        folds += [test_indices[i * length:(i + 1) * length]]
    folds += [test_indices[399 * length:len(test_indices)]]

    print ('len(folds): {}'.format(len(folds)))
    # load saved models
    pp_model = torch.load(args.pp_model)

    # load vocab
    pp_vocab, rev_pp_vocab = cPickle.load(open(args.vocab, 'rb'))

    tag_file_2 = codecs.open(args.parse_vocab_2, 'r', 'utf-8')
    parse_gen_voc_2 = {}
    for idx, line in enumerate(tag_file_2):
        line = line.strip()
        parse_gen_voc_2[line] = idx
    rev_label_voc_2 = dict((v,k) for (k,v) in parse_gen_voc_2.items())

    tag_file_1 = codecs.open(args.parse_vocab_1, 'r', 'utf-8')
    parse_gen_voc_1 = {}
    for idx, line in enumerate(tag_file_1):
        line = line.strip()
        parse_gen_voc_1[round(float(line), 3)] = idx

    # load paraphrase network
    pp_args = pp_model['config_args']
    net = CPGN(pp_args.d_word, pp_args.d_hid, pp_args.d_nt, pp_args.d_nt, pp_args.d_trans, pp_args.d_trans,
        len(pp_vocab), len(parse_gen_voc_1), len(parse_gen_voc_2), pp_args.use_input_parse)


    net.cuda()
    net.load_state_dict(pp_model['state_dict'])
    net.eval()


    # instantiate BPE segmenter
    bpe_codes = codecs.open(args.bpe_codes, encoding='utf-8')
    bpe_vocab = codecs.open(args.bpe_vocab, encoding='utf-8')
    bpe_vocab = read_vocabulary(bpe_vocab, args.bpe_vocab_thresh)
    bpe = BPE(bpe_codes, '@@', bpe_vocab, None)

    # paraphrase the sst!

    fn = ['idx', 'template_B','template_L', 'result_l', 'sentence']

    for fold in folds:
        templates_bl = []

        for i in fold:
            length_l = str(out_lens[i])
            bert_b = round(float(in_bert[i]), 3)
            templates_bl.append(([bert_b], [length_l]))

        # encode templates
        # #1: BERT
        template_1_lens = [1 for x in templates_bl]
        np_templates_1 = np.zeros((len(templates_bl), max(template_1_lens)), dtype='float32')
        for z, template in enumerate(templates_bl):
            np_templates_1[z, :template_1_lens[z]] = [parse_gen_voc_1[w] for w in templates_bl[z][0]]

        tp_templates_1 = Variable(torch.from_numpy(np_templates_1).long().cuda())
        tp_template_lens_1 = torch.from_numpy(np.array(template_1_lens, dtype='int32')).long().cuda()

        # #2: Length

        template_2_lens = [len(x[1]) for x in templates_bl]
        # print ('template_2_lens: {}'.format(template_2_lens))
        np_templates_2 = np.zeros((len(templates_bl), max(template_2_lens)), dtype='int32')
        for z, template in enumerate(templates_bl):
            np_templates_2[z, :template_2_lens[z]] = [parse_gen_voc_2[w] for w in templates_bl[z][1]]

        tp_templates_2 = Variable(torch.from_numpy(np_templates_2).long().cuda())
        tp_template_lens_2 = torch.from_numpy(np.array(template_2_lens, dtype='int32')).long().cuda()

        encode_data(None, tp_templates_1, tp_templates_2, tp_template_lens_1, tp_template_lens_2, fold)
# templates_bert:[[0.662], [0.697], [0.717], [0.737], [0.757], [0.777], [0.797], [0.817], [0.837], [0.857], [0.877], [0.897], [0.917], [0.937], [0.957], [0.977], [0.997], [1.0]]

    hyp_file.close()
    inp_file.close()
    ref_file.close()
