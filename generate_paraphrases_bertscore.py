import torch, time, sys, argparse, os, codecs, h5py, csv
import numpy as np
from torch.autograd import Variable
from nltk import ParentedTree
from train_cpgn_bertscore import SCPN
from subwordnmt.apply_bpe import BPE, read_vocabulary
# from scpn_utils_length import deleaf
# reload(sys)
# sys.setdefaultencoding('utf8')
import pickle as cPickle
# from bert_score import score

import random

# encode sentences and parses for targeted paraphrasing
def encode_data(out_file, tp_templates, tp_template_lens):

    h5f = h5py.File(args.parsed_input_file, 'r')
    inp = h5f['inputs']
    out = h5f['outputs']
    in_lens = h5f['in_lengths']
    out_lens = h5f['out_lengths']

    scores_list = cPickle.load(open('scores_list_scalar.pkl', 'rb'))
    in_bert = scores_list


    with open('paranmt_test_train_dev.pickle', 'rb') as f:
        test_indices = cPickle.load(f)



    print ('len((test_indices)): {}'.format(len((test_indices))))

    print ('data is loaded')
    ref_file = open('evaluation4k/ref_paranmt_B_4k_again.txt', 'w')
    inp_file = open('evaluation4k/inp_paranmt_B_4k_again.txt', 'w')

    ref_dic = {}
    for i in test_indices:
        eos = np.where(out[i] == pp_vocab['EOS'])[0][0]
        ssent = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(out[i, :eos])\
                        if j < out_lens[i]-1])

        ref_dic[i]=ssent
        ref_file.write(ssent)
        ref_file.write('\n')

    fn = ['idx', 'template', 'result', 'sentence']
    ofile = codecs.open(out_file, 'w', 'utf-8')
    out = csv.DictWriter(ofile, delimiter='\t', fieldnames=fn)
    out.writerow(dict((x,x) for x in fn))
    hyp_file = open('evaluation4k/hyp_bert_paranmt_scalaremb_4k_again.txt', 'w')

    cnt = 0
    for i in test_indices:

        stime = time.time()
        input_sentence = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(inp[i])\
                        if j < in_lens[i]-1])

        print ('\n ============ {} ============= '.format(cnt))
        cnt += 1

        print ('input_sentence: {}'.format(input_sentence))
        inp_file.write(input_sentence)
        inp_file.write('\n')

        F1 = round(in_bert[i],3)
        # print ('Gold F1: {}'.format(F1))
        absolute_difference_function = lambda list_value: abs(list_value - F1)
        closest_value = min([x[0] for x in templates_length], key=absolute_difference_function)
        # print ('closest_value: {}'.format(closest_value))


        torch_sent = Variable(torch.from_numpy(np.array(inp[i], dtype='int32')).long().cuda())
        torch_sent_len = torch.from_numpy(np.array([in_lens[i]], dtype='int32')).long().cuda()

        # generate paraphrases from parses
        try:
            beam_dict = net.batch_beam_search(torch_sent.unsqueeze(0), tp_templates,
                torch_sent_len[:], tp_template_lens, pp_vocab['EOS'], beam_size=3, max_steps=40)

            for b_idx in beam_dict:
                prob,_,_,seq = beam_dict[b_idx][0]
                tt = templates_length[b_idx][0]

                gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])

                # P, R, F1 = score([gen_sent], [input_sentence], lang='en', verbose=False)
                # F1 = round(float(F1[0]), 3)
                F1 = '0'
                print (' ')
                print ('GEN: {}'.format(gen_sent))
                print ('INP: {}'.format(input_sentence))
                print ('Template: {}'.format(tt))
                # print ('F1: {}'.format(F1))

                out.writerow({'idx': i,
                    'template': tt,
                    'result': F1,
                    # 'result': result_bert_score,
                    'sentence':gen_sent})

                if closest_value == tt:
                    hyp_file.write(gen_sent)
                    hyp_file.write('\n')

        except Exception as e:
            print (e)

        print (i, time.time() - stime)
    hyp_file.close()
    inp_file.close()
    ref_file.close()

    # with open('output/tt_results.txt', 'w') as f:
    #     for tt in templates_dic:
    #         avg_tt = sum(templates_dic[tt]) / len(templates_dic[tt])
    #         f.write ('\n tt: {} \t avg: {}\n'.format(tt, avg_tt))
    #         f.write ('templates_dic[tt] : {}\n'.format(templates_dic[tt]))
    #         f.write ('len(templates_dic[tt] : {}\n'.format(len(templates_dic[tt])))


    # f_cmu = open('data/result_generate_bert.pkl', 'wb')
    # pkl.dump(input_list, f_cmu)
    # pkl.dump(output_list, f_cmu)
    # pkl.dump(index_list, f_cmu)
    # f_cmu.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Syntactically Controlled Paraphrase Transformer')

    ## paraphrase model args
    parser.add_argument('--gpu', type=str, default='0',
            help='GPU id')
    parser.add_argument('--out_file', type=str, default='output4k/scpn2_bert_scalar_emb_4k.out',
            help='paraphrase save path')
    parser.add_argument('--parsed_input_file', type=str, default='data/parsed_data.h5',
            help='parse load path')
    # parser.add_argument('--parsed_input_file', type=str, default='data/paws.csv',
    #         help='parse load path')
    parser.add_argument('--vocab', type=str, default='data/parse_vocab.pkl',
            help='word vocabulary')
    parser.add_argument('--parse_vocab', type=str, default='data/unique_bert_scalar.txt',
            help='tag vocabulary')
    parser.add_argument('--pp_model', type=str, default='models/scpn2_bert_80_JUNE_scalar_trainedEMB_4k_again.pt',
            help='paraphrase model to load')

    ## BPE args
    parser.add_argument('--bpe_codes', type=str, default='data/bpe.codes')
    parser.add_argument('--bpe_vocab', type=str, default='data/vocab.txt')
    parser.add_argument('--bpe_vocab_thresh', type=int, default=50)

    args = parser.parse_args()

    # os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    # load saved models
    pp_model = torch.load(args.pp_model)

    # load vocab
    pp_vocab, rev_pp_vocab = cPickle.load(open(args.vocab, 'rb'))

    templates_length = []
    # for i in np.arange(0.6, 1.01, 0.025):
    #     templates_length.append([i])

    tag_file = codecs.open(args.parse_vocab, 'r', 'utf-8')
    parse_gen_voc = {}
    for idx, line in enumerate(tag_file):
        line = line.strip()
        parse_gen_voc[round(float(line), 3)] = idx

        if idx % 10 == 0:
            templates_length.append([round(float(line), 3)])
    templates_length.append([1.0])
    print ('templates_length:{} '.format(templates_length))

    rev_label_voc = dict((v, k) for (k, v) in parse_gen_voc.items())
    len_parse_voc = len(parse_gen_voc)

    # load paraphrase network
    pp_args = pp_model['config_args']
    # print 'pp_args.d_nt: {}'.format(pp_args.d_nt)
    # print 'len(parse_gen_voc) - 1: {}'.format(len(parse_gen_voc) - 1)


    net = SCPN(pp_args.d_word, pp_args.d_hid, pp_args.d_nt, pp_args.d_trans,
        len(pp_vocab), len_parse_voc, pp_args.use_input_parse, parse_gen_voc)

    net.cuda()
    net.load_state_dict(pp_model['state_dict'])
    net.eval()

    # encode templates
    template_lens = [1 for x in templates_length]
    np_templates = np.zeros((len(templates_length), max(template_lens)), dtype='float32')
    for z, template in enumerate(templates_length):
        np_templates[z, :template_lens[z]] = [parse_gen_voc[w] for w in templates_length[z]]

    tp_templates = Variable(torch.from_numpy(np_templates).long().cuda())
    tp_template_lens = torch.from_numpy(np.array(template_lens, dtype='int32')).long().cuda()

    # instantiate BPE segmenter
    bpe_codes = codecs.open(args.bpe_codes, encoding='utf-8')
    bpe_vocab = codecs.open(args.bpe_vocab, encoding='utf-8')
    bpe_vocab = read_vocabulary(bpe_vocab, args.bpe_vocab_thresh)
    bpe = BPE(bpe_codes, '@@', bpe_vocab, None)

    # paraphrase the sst!
    encode_data(args.out_file, tp_templates, tp_template_lens)
