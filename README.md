# Controllable Paraphrase Generation Networks (CPGN)

Code to train models from "Controllable Paraphrase Generation with Multiple Types of Constraints".

The code is written in python and requires Pytorch 3.1.


* To **train**, download the training data from https://drive.google.com/file/d/1x1Xz3KQP_Ncu3DVPhOCsAwwOlFgcre5H/view?usp=sharing and move it to the data folder. Then according to your desired constraint, or combination of multiple constraints use the appropriate train_cpgn_\*.py and run it.
Check train_cpgn_*.py for training command line options.
```
python train_cpgn_length.py --data data/parsed_data.h5 --model CPGN_length.pt --gpu 0
```

* In this code we cover the following constraints that you can use instead of '\*' in train_cpgn_*.py: <br />
~ no constraint: baseline <br />
~ single constraints: length, bertscore, parsetree, inpattern, expattern <br />
~ multiple constraints: inpattern_bertscore, inpattern_bertscore_parsetree, inpattern_length, length_bertscore, length_parsetree, parsetree_bertscore <br /> <br />

* To **generate** paraphrases from a set of constraints run generate_paraphrases_*.py (check the script to see available arguments).

## If you use our code or models for your work please cite:
```
@inproceedings{dehghani2021controllable,
  title={Controllable Paraphrase Generationwith Multiple Types of Constraints},
  author={Dehghani, Nazanin and Hajipoor, Hassan and Chevelu, Jonathan and Lecorvé, Gwénolé},
  booktitle={Proceedings of the Controllable Generative Modeling in Language and Vision Workshop at NeurIPS 2021 (CtrlGen)},
  year={2021}
}
```
