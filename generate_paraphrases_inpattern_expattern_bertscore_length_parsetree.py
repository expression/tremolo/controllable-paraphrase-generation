import torch, time, sys, argparse, os, codecs, h5py, csv
import numpy as np
import pickle as cPickle
from torch.autograd import Variable
from train_cpgn_inpattern_bertscore_parsetree import CPGN
from subwordnmt.apply_bpe import BPE, read_vocabulary
from cpgn_utils_inpattern_expattern_bertscore_length_parsetree import *

h5f = h5py.File('data/parsed_data.h5', 'r')
inp = h5f['inputs']
out = h5f['outputs']
in_lens = h5f['in_lengths']
out_lens = h5f['out_lengths']
out_parses = h5f['output_parses']

scores_list = cPickle.load(open('scores_list_scalar.pkl', 'rb'))
in_bert = scores_list

print ('data is loaded')

test_indices = []
ref_file = open('evaluation4k/ref_all2.txt', 'w')
inp_file = open('evaluation4k/inp_all2.txt', 'w')
hyp_file = open('evaluation4k/hyp_all2.txt', 'w')
template_file_pos = open('evaluation4k/template_all2_pos.txt', 'w')
template_file_neg = open('evaluation4k/template_all2_neg.txt', 'w')


def reverse_bpe(sent):
    x = []
    cache = ''

    for w in sent:
        if w.endswith('@@'):
            cache += w.replace('@@', '')
        elif cache != '':
            x.append(cache + w)
            cache = ''
        else:
            x.append(w)

    return ' '.join(x)

# encode sentences and parses for targeted paraphrasing
def encode_data(out_f, tp_templates_1, tp_templates_2, tp_templates_3, tp_templates_4, tp_templates_5,
                tp_template_lens_1, tp_template_lens_2, tp_template_lens_3, tp_template_lens_4, tp_template_lens_5, fold):

    ref_dic = {}
    for i in fold:
        eos = np.where(out[i] == pp_vocab['EOS'])[0][0]
        ssent = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(out[i, :eos])\
                        if j < out_lens[i]-1])

        ref_dic[i]=ssent

    # loop over sentences and transform them
    cnt = 0
    for i in fold:
        # if cnt > CNT:
        #     break
        cnt += 1

        stime = time.time()
        input_sentence = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(inp[i])\
                        if j < in_lens[i]-1])

        print ('\n ============ {}, {} ============= '.format(cnt, i))
        # print ('input: {}'.format(input_sentence))
        # write gold sentence

        # out_f.writerow({'idx': i,
        #               'pattern': 'GOLD', 'length': len(input_sentence.split()),
        #               'sentence': input_sentence})

        torch_sent = Variable(torch.from_numpy(np.array(inp[i], dtype='int32')).long().cuda())
        torch_sent_len = torch.from_numpy(np.array([in_lens[i]], dtype='int32')).long().cuda()

        # generate paraphrases from parses
        try:
            beam_dict = net.batch_beam_search(torch_sent.unsqueeze(0), tp_templates_1, tp_templates_2, tp_templates_3, tp_templates_4, tp_templates_5,
                torch_sent_len[:], tp_template_lens_1, tp_template_lens_2, tp_template_lens_3, tp_template_lens_4, tp_template_lens_5,
                                              pp_vocab['EOS'], beam_size=3, max_steps=40)


            for b_idx in beam_dict:
                prob, _, _, seq = beam_dict[b_idx][0]

                # ##################
                # new gen EOF handling
                gen_sent_list=[]
                for w in seq[:-1]:
                    if rev_pp_vocab[w] == 'EOS':
                        break
                    gen_sent_list.append(rev_pp_vocab[w])
                gen_sent = ' '.join(gen_sent_list)
                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                gen_length = len(gen_sent_list)

                # ##################


                tt_p = templates_pattern_b[b_idx][0]
                tt_reverse = templates_pattern_b[b_idx][1]
                tt_bert = templates_pattern_b[b_idx][2]
                tt_length = templates_pattern_b[b_idx][3]
                tt_parse = templates_pattern_b[b_idx][4]


                # out_f.writerow({'idx': i,
                #               'pattern': tt_p, 'length': tt_l,
                #               'sentence': gen_sent})

                if b_idx == cnt - 1:
                    # print ('===============================================\n')
                    # print ('ref: {}'.format(ref_dic[i]))
                    # print ('pattern: {}'.format(tt_p))
                    # print ('pattern reverse: {}'.format(tt_reverse))
                    # print ('length: {}'.format(tt_length))
                    # print ('bert: {}'.format(tt_bert))
                    # print ('parse: {}'.format(tt_parse))
                    # print ('generated: {}'.format(gen_sent))
                # if len(ref_list[cnt - 1].split()) == tt:
                    hyp_file.write(gen_sent)
                    hyp_file.write('\n')
                    hyp_file.flush()

                    template_file_pos.write(tt_p)
                    template_file_pos.write('\n')
                    template_file_neg.write(tt_reverse)
                    template_file_neg.write('\n')
                    hyp_file.flush()

                    ref_file.write(ref_dic[i])
                    ref_file.write('\n')

                    inp_file.write(input_sentence)
                    inp_file.write('\n')

                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                # out.writerow({'idx': ex['idx'],
                #     'template':tt, 'generated_length':len(seq[:-1]),
                #     'sentence':reverse_bpe(gen_sent.split())})

        except Exception as e:
            print (e)
            print ('beam search OOM')

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Syntactically Controlled Paraphrase Transformer')

    ## paraphrase model args
    parser.add_argument('--gpu', type=str, default='1',
                        help='GPU id')
    parser.add_argument('--out_file', type=str, default='output4k/CPGN_blank_2_paranmt_B_4k.out',
                        help='paraphrase save path')
    parser.add_argument('--parsed_input_file', type=str, default='data/parsed_data.h5',
                        help='parse load path')
    parser.add_argument('--vocab', type=str, default='data/parse_vocab.pkl',
                        help='word vocabulary')

    parser.add_argument('--parse_vocab_3', type=str, default='data/unique_bert_scalar.txt',
            help='tag vocabulary 3')
    parser.add_argument('--parse_vocab_4', type=str, default='data/unique_lenght.txt',
            help='tag vocabulary 4')
    parser.add_argument('--parse_vocab_5', type=str, default='data/ptb_tagset.txt',
            help='tag vocabulary 5')
    parser.add_argument('--pp_model', type=str, default='models/CPGN2_B_pattern_new_4k_L_Parse_all.pt',
                        help='paraphrase model to load')

    ## BPE args
    parser.add_argument('--bpe_codes', type=str, default='data/bpe.codes')
    parser.add_argument('--bpe_vocab', type=str, default='data/vocab.txt')
    parser.add_argument('--bpe_vocab_thresh', type=int, default=50)

    args = parser.parse_args()

    # os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    modell = args.pp_model
    slash = modell.find('/')+1
    modelll_name = modell[slash :]
    with open('paranmt_test_train_dev.pickle', 'rb') as f:
        test_indices = cPickle.load(f)
        _ = cPickle.load(f)

    length = int(len(test_indices) / 400)  # length of each fold : 64
    folds = []
    for i in range(399):
        folds += [test_indices[i * length:(i + 1) * length]]
    folds += [test_indices[399 * length:len(test_indices)]]

    print ('len(folds): {}'.format(len(folds)))

    # load saved models
    pp_model = torch.load(args.pp_model)

    # load vocab
    pp_vocab, rev_pp_vocab = cPickle.load(open(args.vocab, 'rb'))
    rev_pp_vocab[len(rev_pp_vocab)] = '___'
    pp_vocab['___'] = len(pp_vocab)

    label_voc_1 =pp_vocab
    rev_label_voc_1 = rev_pp_vocab

    label_voc_2 = pp_vocab
    rev_label_voc_2 = rev_pp_vocab

    tag_file_3 = codecs.open(args.parse_vocab_3, 'r', 'utf-8')
    label_voc_3 = {}
    for idx, line in enumerate(tag_file_3):
        line = line.strip()
        label_voc_3[float(line)] = idx
    rev_label_voc_3 = dict((v,k) for (k,v) in label_voc_3.items())

    tag_file_4 = codecs.open(args.parse_vocab_4, 'r', 'utf-8')
    label_voc_4 = {}
    for idx, line in enumerate(tag_file_4):
        line = line.strip()
        label_voc_4[line] = idx
    rev_label_voc_4 = dict((v,k) for (k,v) in label_voc_4.items())

    tag_file_5 = codecs.open(args.parse_vocab_5, 'r', 'utf-8')
    label_voc_5 = {}
    for idx, line in enumerate(tag_file_5):
        line = line.strip()
        label_voc_5[line] = idx
    rev_label_voc_5 = dict((v,k) for (k,v) in label_voc_5.items())

    len_voc = len(pp_vocab)

    len_parse_voc_1 = len(label_voc_1)
    len_parse_voc_2 = len(label_voc_2)
    len_parse_voc_3 = len(label_voc_3)
    len_parse_voc_4 = len(label_voc_4)
    len_parse_voc_5 = len(label_voc_5)

    # load paraphrase network
    pp_args = pp_model['config_args']
    net = CPGN(pp_args.d_word, pp_args.d_hid, pp_args.d_nt, pp_args.d_nt, pp_args.d_nt, pp_args.d_nt, pp_args.d_nt,
               pp_args.d_trans, pp_args.d_trans, pp_args.d_trans, pp_args.d_trans, pp_args.d_trans,
    len_voc, len_parse_voc_1, len_parse_voc_2, len_parse_voc_3, len_parse_voc_4, len_parse_voc_5, pp_args.use_input_parse)


    net.cuda()
    net.load_state_dict(pp_model['state_dict'])
    net.eval()

    fn = ['idx', 'pattern', 'length', 'sentence']
    ofile = codecs.open(args.out_file, 'w', 'utf-8')

    out_f = csv.DictWriter(ofile, delimiter='\t', fieldnames=fn)
    out_f.writerow(dict((x, x) for x in fn))

    for fold in folds:

        # encode templates
        # template_lens = ['___ ___ ___ ___ ___' for x in range(CNT)]
        templates_pattern_b = []

        # np_templates = np.zeros((len(fold), 5), dtype='int32')
        # counter = 0
        for i in fold:
            input_i = inp[i]
            output_i = out[i]

            flag_j = False
            flag_k = False
            for j in range(out_lens[i]):
                if output_i[j] not in input_i:
                    flag_j = True
                    for k in range(j + 1, out_lens[i]):
                        if output_i[k] not in input_i:
                            flag_k = True

                            if j == 0:
                                if k == j + 1:
                                    res = rev_pp_vocab[output_i[j]] + ' ' + rev_pp_vocab[output_i[k]] + ' ___'
                                else:
                                    if k == out_lens[i] - 1:
                                        res = rev_pp_vocab[output_i[j]] + ' ___ ' + rev_pp_vocab[output_i[k]]
                                    else:
                                        res = rev_pp_vocab[output_i[j]] + ' ___ ' + rev_pp_vocab[output_i[k]] + ' ___'
                            else:
                                if k == j + 1 and k == out_lens[i] - 1:
                                    res = '___ ' + rev_pp_vocab[output_i[j]] + ' ' + rev_pp_vocab[output_i[k]]
                                elif k == j + 1:
                                    res = '___ ' + rev_pp_vocab[output_i[j]] + ' ' + rev_pp_vocab[output_i[k]] + ' ___'
                                else:
                                    if k == out_lens[i] - 1:
                                        res = '___ ' + rev_pp_vocab[output_i[j]] + ' ___ ' + rev_pp_vocab[output_i[k]]
                                    else:
                                        res = '___ ' + rev_pp_vocab[output_i[j]] + ' ___ ' + rev_pp_vocab[
                                            output_i[k]] + ' ___'
                            break

                    if not flag_k:
                        if j == 0:
                            res = rev_pp_vocab[output_i[j]] + ' ___'
                        elif j == out_lens[i] - 1:
                            res = '___ ' + rev_pp_vocab[output_i[j]]
                        else:
                            res = '___ ' + rev_pp_vocab[output_i[j]] + ' ___'
                    break

            if not flag_j:
                res = '___'




            input_i = inp[i]
            output_i = out[i]
            res_reverse = ''
            flag_j = False
            flag_k = False
            for j in range(in_lens[i]):
                if input_i[j] not in output_i:
                    flag_j = True
                    for k in range(j + 1, in_lens[i]):
                        if input_i[k] not in output_i:
                            flag_k = True

                            res_reverse = rev_pp_vocab[input_i[j]] + ' ' + rev_pp_vocab[input_i[k]]

                            break

                    if not flag_k:
                        res_reverse = rev_pp_vocab[input_i[j]]
                    break

            if not flag_j:
                fold.remove(i)
                continue
                # res = ' '

            out_tree = ParentedTree.fromstring(out_parses[i])


            if pp_args.tree_dropout > 0:
                tree_dropout(out_tree, pp_args.tree_dropout, 0)
            elif pp_args.tree_level_dropout > 0:
                parse_tree_level_dropout(out_tree, pp_args.tree_level_dropout)

            out_full_trans = deleaf(out_tree)

            templates_pattern_b.append((str(res), str(res_reverse), [round(float(in_bert[i]), 3)], [str(out_lens[i])], out_full_trans))


        # encode templates
        # #1: pattern
        template_1_lens = [len(x[0].split(' ')) for x in templates_pattern_b]
        np_templates_1 = np.zeros((len(templates_pattern_b), max(template_1_lens)), dtype='int32')
        for z, template in enumerate(templates_pattern_b):
            np_templates_1[z, :template_1_lens[z]] = [label_voc_1[w] for w in templates_pattern_b[z][0].split(' ')]

        tp_templates_1 = Variable(torch.from_numpy(np_templates_1).long().cuda())
        tp_template_lens_1 = torch.from_numpy(np.array(template_1_lens, dtype='int32')).long().cuda()

        # #1: pattern rev
        template_2_lens = [len(x[1].split(' ')) for x in templates_pattern_b]
        np_templates_2 = np.zeros((len(templates_pattern_b), max(template_2_lens)), dtype='int32')
        for z, template in enumerate(templates_pattern_b):
            np_templates_2[z, :template_2_lens[z]] = [label_voc_2[w] for w in templates_pattern_b[z][1].split(' ')]

        tp_templates_2 = Variable(torch.from_numpy(np_templates_2).long().cuda())
        tp_template_lens_2 = torch.from_numpy(np.array(template_2_lens, dtype='int32')).long().cuda()

        # #3: bert
        template_3_lens = [1 for x in templates_pattern_b]
        np_templates_3 = np.zeros((len(templates_pattern_b), max(template_3_lens)), dtype='float32')
        for z, template in enumerate(templates_pattern_b):
            np_templates_3[z, :template_3_lens[z]] = [label_voc_3[w] for w in templates_pattern_b[z][2]]

        tp_templates_3 = Variable(torch.from_numpy(np_templates_3).long().cuda())
        tp_template_lens_3 = torch.from_numpy(np.array(template_3_lens, dtype='int32')).long().cuda()

        # #4: length
        template_4_lens = [1 for x in templates_pattern_b]
        np_templates_4 = np.zeros((len(templates_pattern_b), max(template_4_lens)), dtype='int32')
        for z, template in enumerate(templates_pattern_b):
            np_templates_4[z, :template_4_lens[z]] = [label_voc_4[w] for w in templates_pattern_b[z][3]]

        tp_templates_4 = Variable(torch.from_numpy(np_templates_4).long().cuda())
        tp_template_lens_4 = torch.from_numpy(np.array(template_4_lens, dtype='int32')).long().cuda()


        # #5: parse
        template_5_lens = [len(x[4]) for x in templates_pattern_b]
        np_templates_5 = np.zeros((len(templates_pattern_b), max(template_5_lens)), dtype='int32')
        for z, template in enumerate(templates_pattern_b):
            np_templates_5[z, :template_5_lens[z]] = [label_voc_5[w] for w in templates_pattern_b[z][4]]

        tp_templates_5 = Variable(torch.from_numpy(np_templates_5).long().cuda())
        tp_template_lens_5 = torch.from_numpy(np.array(template_5_lens, dtype='int32')).long().cuda()




        # instantiate BPE segmenter
        bpe_codes = codecs.open(args.bpe_codes, encoding='utf-8')
        bpe_vocab = codecs.open(args.bpe_vocab, encoding='utf-8')
        bpe_vocab = read_vocabulary(bpe_vocab, args.bpe_vocab_thresh)
        bpe = BPE(bpe_codes, '@@', bpe_vocab, None)

        # paraphrase the sst!
        encode_data(out_f, tp_templates_1, tp_templates_2, tp_templates_3, tp_templates_4, tp_templates_5,
                    tp_template_lens_1, tp_template_lens_2, tp_template_lens_3, tp_template_lens_4, tp_template_lens_5, fold)

        # break

    hyp_file.close()
    template_file_pos.close()
    template_file_neg.close()
    ref_file.close()
    inp_file.close()
