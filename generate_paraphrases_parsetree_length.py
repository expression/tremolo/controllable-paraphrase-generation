import torch, time, sys, argparse, os, codecs, h5py, csv
import numpy as np
import pickle as cPickle
from torch.autograd import Variable
from train_cpgn_length_parsetree import CPGN
from subwordnmt.apply_bpe import BPE, read_vocabulary
from nltk import ParentedTree
from cpgn_utils_parsetree import deleaf, parse_indexify_transformations

h5f = h5py.File('data/parsed_data.h5', 'r')
inp = h5f['inputs']
out = h5f['outputs']
in_lens = h5f['in_lengths']
out_lens = h5f['out_lengths']
in_parses = h5f['input_parses']
out_parses = h5f['output_parses']

print ('data is loaded')

test_indices = []
ref_file = open('evaluation4k/ref_paranmt_parse_L_4k_2.txt', 'w')
inp_file = open('evaluation4k/inp_paranmt_parse_L_4k_2.txt', 'w')
hyp_file = open('evaluation4k/hyp_parse_paranmt_L_4k_2.txt', 'w')
template_file = open('evaluation4k/template_parse_paranmt_L_4k_2.txt', 'w')


def reverse_bpe(sent):
    x = []
    cache = ''

    for w in sent:
        if w.endswith('@@'):
            cache += w.replace('@@', '')
        elif cache != '':
            x.append(cache + w)
            cache = ''
        else:
            x.append(w)

    return ' '.join(x)

# encode sentences and parses for targeted paraphrasing
def encode_data(out_f, tp_templates_1, tp_templates_2, tp_template_lens_1, tp_template_lens_2, fold):

    ref_dic = {}
    for i in fold:
        eos = np.where(out[i] == pp_vocab['EOS'])[0][0]
        ssent = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(out[i, :eos])\
                        if j < out_lens[i]-1])

        ref_dic[i]=ssent
        ref_file.write(ssent)
        ref_file.write('\n')

    # loop over sentences and transform them
    cnt = 0
    for i in fold:
        # if cnt > CNT:
        #     break
        cnt += 1

        stime = time.time()
        input_sentence = ' '.join([rev_pp_vocab[w] for (j, w) in enumerate(inp[i])\
                        if j < in_lens[i]-1])
        inp_file.write(input_sentence)
        inp_file.write('\n')

        print ('\n ============ {}, {} ============= '.format(cnt, i))
        print ('input: {}'.format(input_sentence))
        # write gold sentence

        out_f.writerow({'idx': i,
                      'pattern': 'GOLD', 'length': len(input_sentence.split()),
                      'sentence': input_sentence})

        torch_sent = Variable(torch.from_numpy(np.array(inp[i], dtype='int32')).long().cuda())
        torch_sent_len = torch.from_numpy(np.array([in_lens[i]], dtype='int32')).long().cuda()

        # generate paraphrases from parses
        try:
            beam_dict = net.batch_beam_search(torch_sent.unsqueeze(0), tp_templates_1, tp_templates_2,
                torch_sent_len[:], tp_template_lens_1, tp_template_lens_2, pp_vocab['EOS'], beam_size=3, max_steps=40)

            for b_idx in beam_dict:
                prob, _, _, seq = beam_dict[b_idx][0]

                # ##################
                # new gen EOF handling
                gen_sent_list=[]
                for w in seq[:-1]:
                    if rev_pp_vocab[w] == 'EOS':
                        break
                    gen_sent_list.append(rev_pp_vocab[w])
                gen_sent = ' '.join(gen_sent_list)
                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                gen_length = len(gen_sent_list)

                # ##################
                tt_p = templates_parse_l[b_idx][0]
                tt_l = templates_parse_l[b_idx][1][0]
                # out_f.writerow({'idx': i,
                #               'pattern': tt_p, 'length': tt_l,
                #               'sentence': gen_sent})

                if b_idx == cnt - 1:
                    # print ('===============================================\n')
                    print ('ref: {}'.format(ref_dic[i]))
                    print ('parse: {}'.format(tt_p))
                    print ('length: {}'.format(tt_l))
                    print ('generated: {}'.format(gen_sent))
                # if len(ref_list[cnt - 1].split()) == tt:
                    hyp_file.write(gen_sent)
                    hyp_file.write('\n')
                    hyp_file.flush()

                    template_file.write(' '.join(tt_p))
                    template_file.write('\n')
                    hyp_file.flush()

                # gen_sent = ' '.join([rev_pp_vocab[w] for w in seq[:-1]])
                # out.writerow({'idx': ex['idx'],
                #     'template':tt, 'generated_length':len(seq[:-1]),
                #     'sentence':reverse_bpe(gen_sent.split())})

        except Exception as e:
            print (e)
            print ('beam search OOM')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Syntactically Controlled Paraphrase Transformer')

    ## paraphrase model args
    parser.add_argument('--gpu', type=str, default='1',
                        help='GPU id')
    parser.add_argument('--out_file', type=str, default='output4k/CPGN_paranmt_parse_L_4k.out',
                        help='paraphrase save path')
    parser.add_argument('--parsed_input_file', type=str, default='data/parsed_data.h5',
                        help='parse load path')
    parser.add_argument('--vocab', type=str, default='data/parse_vocab.pkl',
                        help='word vocabulary')
    parser.add_argument('--parse_vocab_1', type=str, default='data/ptb_tagset.txt',
            help='tag vocabulary 1')
    parser.add_argument('--parse_vocab_2', type=str, default='data/unique_lenght.txt',
            help='tag vocabulary 2')

    parser.add_argument('--pp_model', type=str, default='models/CPGN2_L_P_new_4k.pt',
                        help='paraphrase model to load')

    ## BPE args
    parser.add_argument('--bpe_codes', type=str, default='data/bpe.codes')
    parser.add_argument('--bpe_vocab', type=str, default='data/vocab.txt')
    parser.add_argument('--bpe_vocab_thresh', type=int, default=50)

    args = parser.parse_args()
    modell = args.pp_model
    slash = modell.find('/')+1
    modelll_name = modell[slash :]
    with open('minibatches/test_minibatches_' + modelll_name + '.txt', 'r') as f:
        for line in f.readlines():
            [start, end] = [int(x) for x in line.strip().split(',')]
            for i in range(start, end):
                test_indices.append(i)

    length = int(len(test_indices) / 200)  # length of each fold : 64
    folds = []
    for i in range(199):
        folds += [test_indices[i * length:(i + 1) * length]]
    folds += [test_indices[199 * length:len(test_indices)]]

    # load saved models
    pp_model = torch.load(args.pp_model)

    # load vocab
    pp_vocab, rev_pp_vocab = cPickle.load(open(args.vocab, 'rb'))

    tag_file_1 = codecs.open(args.parse_vocab_1, 'r', 'utf-8')
    parse_gen_voc_1 = {}
    for idx, line in enumerate(tag_file_1):
        line = line.strip()
        parse_gen_voc_1[line] = idx
    rev_label_voc = dict((v, k) for (k, v) in parse_gen_voc_1.items())

    tag_file_2 = codecs.open(args.parse_vocab_2, 'r', 'utf-8')
    parse_gen_voc_2 = {}
    for idx, line in enumerate(tag_file_2):
        line = line.strip()
        parse_gen_voc_2[line] = idx
    rev_label_voc_2 = dict((v,k) for (k,v) in parse_gen_voc_2.items())

    # load paraphrase network
    pp_args = pp_model['config_args']
    net = CPGN(pp_args.d_word, pp_args.d_hid, pp_args.d_nt, 56, pp_args.d_trans, 128,
        len(pp_vocab), len(parse_gen_voc_1), len(parse_gen_voc_2), pp_args.use_input_parse)

    net.cuda()
    net.load_state_dict(pp_model['state_dict'])
    net.eval()

    fn = ['idx', 'pattern', 'length', 'sentence']
    ofile = codecs.open(args.out_file, 'w', 'utf-8')

    out_f = csv.DictWriter(ofile, delimiter='\t', fieldnames=fn)
    out_f.writerow(dict((x, x) for x in fn))

    for fold in folds:
        templates_parse_l = []
        for i in fold:
            out_tree = ParentedTree.fromstring(out_parses[i])

            out_full_trans = deleaf(out_tree)
            templates_parse_l.append((out_full_trans, [str(out_lens[i])]))

        # encode templates
        # #1: pattern
        # template_1_lens = [6 for x in templates_pattern_l]
        template_1_lens = [len(x[0]) for x in templates_parse_l]
        np_templates_1 = np.zeros((len(templates_parse_l), max(template_1_lens)), dtype='int32')
        for z, template in enumerate(templates_parse_l):
            np_templates_1[z, :template_1_lens[z]] = [parse_gen_voc_1[w] for w in templates_parse_l[z][0]]

        tp_templates_1 = Variable(torch.from_numpy(np_templates_1).long().cuda())
        tp_template_lens_1 = torch.from_numpy(np.array(template_1_lens, dtype='int32')).long().cuda()

        # #2: Length
        template_2_lens = [1 for x in templates_parse_l]
        # print 'template_2_lens: {}'.format(template_2_lens)
        np_templates_2 = np.zeros((len(templates_parse_l), max(template_2_lens)), dtype='int32')
        for z, template in enumerate(templates_parse_l):
            np_templates_2[z, :template_2_lens[z]] = [parse_gen_voc_2[w] for w in templates_parse_l[z][1]]

        tp_templates_2 = Variable(torch.from_numpy(np_templates_2).long().cuda())
        tp_template_lens_2 = torch.from_numpy(np.array(template_2_lens, dtype='int32')).long().cuda()

        # instantiate BPE segmenter
        bpe_codes = codecs.open(args.bpe_codes, encoding='utf-8')
        bpe_vocab = codecs.open(args.bpe_vocab, encoding='utf-8')
        bpe_vocab = read_vocabulary(bpe_vocab, args.bpe_vocab_thresh)
        bpe = BPE(bpe_codes, '@@', bpe_vocab, None)

        # paraphrase the sst!
        encode_data(out_f, tp_templates_1, tp_templates_2, tp_template_lens_1, tp_template_lens_2, fold)

    hyp_file.close()
    template_file.close()
    ref_file.close()
    inp_file.close()
